FROM ubuntu:disco

RUN DEBIAN_FRONTEND=nointeractive apt-get update && apt-get install -y --no-install-recommends \
    cython python3 python3-pip python3-dev python3-setuptools \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean

# install the notebook package
RUN pip3 install --upgrade pip && \
    pip3 install pyzmq --install-option="--zmq=bundled" && \
    pip3 install notebook